package me.aubury.griefpreventionannouncement;

import com.sun.tools.javac.util.StringUtils;
import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.StringUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public final class Griefpreventionannouncement extends JavaPlugin implements Listener {
    GriefPrevention gp;

    HashMap<UUID, Long> userClaimMap;

    @Override
    public void onEnable() {
        // Plugin startup logic
        gp = (GriefPrevention) getServer().getPluginManager().getPlugin("GriefPrevention");
        getServer().getPluginManager().registerEvents(this, this);
        userClaimMap = new HashMap<>();
        saveDefaultConfig();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent e){
        Player p = e.getPlayer();
        Claim claim = gp.dataStore.getClaimAt(p.getLocation(),false,null);
        if(claim != null){
            if(userClaimMap.containsKey(p.getUniqueId())){
                if(claim.getID() != userClaimMap.get(p.getUniqueId())){
                    userClaimMap.remove(p.getUniqueId());
                    userClaimMap.put(p.getUniqueId(),claim.getID());
                    sendWelcomeMessage(claim.getID(),p);
                    //User has changed claim, send the new message.
                }
                //User is in the same claim
                return;
            }
            // User has entered a new claim send the message.
            sendWelcomeMessage(claim.getID(),p);
            userClaimMap.put(p.getUniqueId(),claim.getID());
        }else{
            if(userClaimMap.containsKey(p.getUniqueId())){
                //User has left the claim send the message.
                sendExitMessage(userClaimMap.get(p.getUniqueId()),p);
                userClaimMap.remove(p.getUniqueId());

            }
        }
    }
    public void sendWelcomeMessage(long claimId, Player p){
        Claim c = gp.dataStore.getClaim(claimId);
        String prefix = getConfig().getString("prefix");


        String[] managers = c.managers.toArray(new String[c.managers.size()]);

        String managersNames = String.join(", ", managers);

        List<String> entryMessage = new ArrayList<>();
        for (String line: getConfig().getStringList("enter-message")) {
            entryMessage.add(ChatColor.translateAlternateColorCodes('&', prefix+(line)
                .replace("{username}",c.getOwnerName())
                .replace("{trusted}",managersNames)
        ));
       }
        for (String line: entryMessage) {
            p.sendMessage(line);
        }
    }

    public void sendExitMessage(long claimId, Player p){
        Claim c = gp.dataStore.getClaim(claimId);
        String prefix = getConfig().getString("prefix");

        String[] managers = c.managers.toArray(new String[c.managers.size()]);
        String managersNames = String.join(", ", managers);

        List<String> exitMessage = new ArrayList<>();
        for (String line: getConfig().getStringList("exit-message")) {
            exitMessage.add(ChatColor.translateAlternateColorCodes('&', prefix+(line)
                    .replace("{username}",c.getOwnerName())
                    .replace("{trusted}",managersNames)
            ));
        }

        for (String line: exitMessage) {
            p.sendMessage(line);
        }
    }

}
